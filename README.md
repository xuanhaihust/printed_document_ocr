## VB2 - Semester 2 - Project 1

- Topic: OCR for printed sheet
- Input: printed page image
- Output: text on that page

- Pre-trained model: CRNN-CTC on pytorch by this repo: https://github.com/pbcquoc/crnn
- Image preprocessing with OpenCV

This's a small project which aimed for an understanding of common processes in an OCR solution.



