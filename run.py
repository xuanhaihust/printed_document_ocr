import argparse, os
import cv2
from worker import main

while True:
    img_path = input("Images: ")
    if not os.path.isfile(img_path):
        print ("Oops. It looks like your input is incorrect. Please try again!")
        continue
    img = cv2.imread(img_path)

    main(img)