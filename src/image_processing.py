#!/usr/bin/python
# encoding: utf-8

import matplotlib.pyplot as plt
import cv2
import numpy as np
from PIL import Image, ImageFont, ImageDraw

import os, json, shutil, re


def show_images(*images, size=10):
    fig=plt.figure(figsize=(size, size))

    for i, img in enumerate(images):
        if len(img.shape) < 3:
            plt.subplot(int("1"+str(len(images))+str(i+1))),plt.imshow(img, cmap=plt.cm.gray)
        else:
            plt.subplot(int("1"+str(len(images))+str(i+1))),plt.imshow(img)
    plt.show()

def save_images(*images, prefix_name='', extension = '.jpg', folder_path='.'):
    try:
        for i, img in enumerate(images):
            file_name = prefix_name + str(i) + extension
            cv2.imwrite(folder_path + '/' + file_name, img)
    except:
        print('Oops, something wrong with the input images or folder_path is not exist.')

def denoise_binary_converter(image, blur = True):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    if blur:
        gray = cv2.GaussianBlur(gray, (7,7), 0)
    # thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
    thresh = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
                cv2.THRESH_BINARY_INV,5,2)

    thresh = cv2.fastNlMeansDenoising(thresh, thresh, 50, 7, 21)
    _, thresh = cv2.threshold(thresh, 200, 255, cv2.THRESH_BINARY)

    return thresh

def find_paragraph_contours(binary_image, return_dilate=False):
    # Create rectangular structuring element and dilate
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7,7))
    dilate = cv2.dilate(binary_image, kernel, iterations=4)

    # Find contours and draw rectangle
    cnts, hierarchy = cv2.findContours(dilate, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    if return_dilate:
        return cnts, hierarchy, dilate
    else:
        return cnts, hierarchy

def sort_contours(cnts, method="top-to-bottom"):
    # initialize the reverse flag and sort index
    reverse = False
    i = 0
    # handle if we need to sort in reverse
    if method == "right-to-left" or method == "left-to-right":
        reverse = True
    # handle if we are sorting against the y-coordinate rather than
    # the x-coordinate of the bounding box
    if method == "top-to-bottom" or method == "bottom-to-top":
        i = 1
    # construct the list of bounding boxes and sort them from top to
    # bottom
    boundingBoxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
        key=lambda b:b[1][i], reverse=reverse))
    # return the list of sorted contours and bounding boxes
    return (cnts, boundingBoxes)


def draw_contours_bounding_box(image, contours, color=(0,255,0)):
    for c in contours:
        x, y, w, h = cv2.boundingRect(c)
        cv2.rectangle(image, (x, y), (x + w, y + h), color, 2)


def contour_rotated_box(image, contours, color = (0,255,0), draw=True):
    """Draw the align-rotated bounding box for contours on image
    return contour box list (coordinates)
    """
    contour_box_list = []
    for contour in contours:
        rbox = cv2.minAreaRect(contour)
        pts = cv2.boxPoints(rbox)
        pts = np.intp(pts)
        contour_box_list.append(pts)
        if draw:
            cv2.drawContours(image, [pts], -1, color, 1, cv2.LINE_AA)
    return contour_box_list

def order_points(pts):
    """return ordered coordinates of 4 points of a rectangle box in clock-wise, that:
    rect[0]: top-left
    rect[1]: top-right
    rect[2]: bottom-right
    rect[3]: bottom-left
    """
    rect = np.zeros((4, 2), dtype = "float32")
    # the top-left point will have the smallest sum, whereas
    # the bottom-right point will have the largest sum
    s = pts.sum(axis = 1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]
    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis = 1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]
    # return the ordered coordinates
    return rect

def object_extraction(sence_image, object_points, inter=cv2.INTER_NEAREST):
    """Extract and align object in image
    Input: image contains object, 4 points of object conner
    Output: image of object only
    """

    pts1 = order_points(object_points)
    h = int(np.linalg.norm(pts1[0]-pts1[3]))
    w = int(np.linalg.norm(pts1[0]-pts1[1]))

    pts2 = np.float32([[0,0],[w,0],[w,h],[0,h]])
    M = cv2.getPerspectiveTransform(pts1,pts2)
    dst = cv2.warpPerspective(sence_image,M,(w, h), inter)

    return dst

def paragraph_box_extracting(binary_image):
    """Extract paragraph regions from text image
    input: binary image
    output: list of paragraph region coordinates: [origin_x, origin_y, w, h]
    """
    paragraph_cnts, _ = find_paragraph_contours(binary_image)
    paragraph_cnts, _ = sort_contours(paragraph_cnts, method="top-to-bottom")

    para_boxes = contour_rotated_box(binary_image, paragraph_cnts, color=(255,0,0), draw=False) # red boxes
    para_boxes_sorted = []
    for box in para_boxes:    
        points = order_points(box)
        para_boxes_sorted.append(points)

    return para_boxes_sorted

def paragraph_extracting(image):
    """
    input: text page image
    output: image of paragraphs extracted from input image
    """
    para_images = []
    binary_image = denoise_binary_converter(image, blur=True)

    paragraph_cnts, _ = find_paragraph_contours(binary_image)
    paragraph_cnts, _ = sort_contours(paragraph_cnts, method="top-to-bottom")

    para_boxes = contour_rotated_box(binary_image, paragraph_cnts, color=(255,0,0), draw=False) # red boxes

    for box in para_boxes:
        box_sorted = order_points(box)
        para_image = object_extraction(image, box)
        para_images.append(para_image)

    return para_images

def line_segmentating(single_paragraph_image, draw=False, return_dilate=False):
    """
    Extract lines from paragraph image
    Input: Single paragraph image
    Output: list of line images (list of numpy array) ordered top-to-bottom
    """
    image = single_paragraph_image.copy()
    thresh = denoise_binary_converter(image, blur=True)

    # dilation for line
    kernel = np.ones((3, 50), np.uint8) # the kernel mainly expand in horizontal axis
    img_dilation = cv2.dilate(thresh, kernel, iterations=1)

    #find contours
    ctrs, hier = cv2.findContours(img_dilation, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    #sort contours
    # sorted_ctrs = sorted(ctrs, key=lambda ctr: cv2.boundingRect(ctr)[0])
    sorted_contours, _ = sort_contours(ctrs, method="top-to-bottom")

    lines = contour_rotated_box(single_paragraph_image, sorted_contours, draw=draw, color=(255,0,0))

    line_rois = []
    for line in lines:
        line_rois.append(object_extraction(image, line))

    output = line_rois

    if return_dilate:
        output = line_rois, img_dilation

    return output

def line_extractor_ulti(page_image, to_pillow=False):
    """
    Extract line images from page image
    Input: page image
    Output: dictionary of lines of each paragraph in input page image
    """
    para_images = paragraph_extracting(page_image)
    line_image_dict = {}
    for i, para in enumerate(para_images):
        para_name = 'para_' + str(i)
        lines = line_segmentating(para, draw=False)
        if to_pillow:
            lines = list(map(Image.fromarray(), lines))
        line_image_dict[para_name] = lines

    return line_image_dict
