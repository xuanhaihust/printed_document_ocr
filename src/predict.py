import torch
from torch.autograd import Variable
from src.utils import strLabelConverter, resizePadding
from PIL import Image
import sys
import src.crnn as crnn
import argparse
from torch.nn.functional import softmax
import numpy as np
import time

ALPHABET = open('lib/char', encoding='utf8').read().rstrip()

def predict(image, model, output_path=None, imgW=None, imgH=32, verbose=False):
    """
    Input: line image (PIL image object)
    Output: predicted text and its probability
    if output_folder is not None, save .txt file to that folder
    ------
    **Parameters**:
    - img_path: path to color image to be predicted
    - model: model which will be used for prediction
    - output_path: the folder in which output .txt file will save
    - imgW: the width of input image. If None, it'll be calculated autotically
    - imgH: the height of input image. Default=32.
    - return_raw: return raw string
    """
    if verbose:
        start_time = time.time()
        
    converter = strLabelConverter(ALPHABET, ignore_case=False)
    
    if type(image).__module__ == np.__name__: # if numpy array, convert to PIL image instance
        image = Image.fromarray(image)
    
    image = image.convert('RGB')
    image = resizePadding(image, imgW, imgH)

    if torch.cuda.is_available():
        image = image.cuda()

    image = image.view(1, *image.size())
    image = Variable(image)

    model.eval()

    start_time = time.time()
    preds = model(image)

    values, prob = softmax(preds, dim=-1).max(2)
    preds_idx = (prob > 0).nonzero()
    sent_prob = values[preds_idx[:,0], preds_idx[:, 1]].mean().item()

    _, preds = preds.max(2)
    preds = preds.transpose(1, 0).contiguous().view(-1)
    preds_size = Variable(torch.IntTensor([preds.size(0)]))
    raw_pred = converter.decode(preds.data, preds_size.data, raw=True)
    sim_pred = converter.decode(preds.data, preds_size.data, raw=False)
    
    if verbose:
        print('%-20s => %-20s : prob: %s time: %s' % (raw_pred, sim_pred, sent_prob, 
                                                      time.time() - start_time))
    if output_path is not None: # export .txt file
        image_file_name = os.path.split(img_path)[1] # os.path.split return (folder_path, filename)
        text_file_path = os.path.join(output_path, image_file_name[:-4] + '.txt')
        with open(text_file_path, 'w', encoding='utf8') as f: # output_path/image_name.txt
            f.write(sim_pred)
    
    return sim_pred

def predict_batch(image_list, model , output_file_path=None,  imgW=None, imgH=32):
    """
    Predict on batch of lines
    Input: list of line images
    Output: list of text lines
    ------
    Parameters:
    - line_image_list: list of line images (3D numpy array)
    - output_name: the file.txt in which output will be saved (all texts)
    - imgW: the width of input image. If None, it'll be calculated autotically
    - imgH: the height of input image. Default=32.
    """
    predict_func = lambda img:predict(img, model)
    text_list = list(map(predict_func, image_list))
    
    if output_file_path is not None:
        with open(output_file_path, 'w', encoding='utf8') as f:
            for line in text_list:
                f.write(line + '\n')
    
    return text_list

def predict_ulti(line_image_dict, model, alphabet, output_file_path=None,  imgW=None, imgH=32):
    """
    Predict text for full page image
    Input: line image dictionary taken from image processing section
    Output: text dictionary
    ------
    Parameters:
    - line_image_list: line image dict which contain list of line images (3D numpy array) for each key (paragraph)
    - output_name: the file.txt in which output will be saved (all texts)
    - imgW: the width of input image. If None, it'll be calculated autotically
    - imgH: the height of input image. Default=32.
    - model_path: path to CRNN-CTC model
    """
    
    line_text_dict = {}
    for para, image_list in line_image_dict.items():
        text_list = predict_batch(image_list, model , output_file_path=None,  imgW=None, imgH=imgH)
        line_text_dict[para] = text_list
        
    if output_file_path is not None:
        with open(output_file_path, 'w', encoding='utf8') as f:
            for para, text_list in line_text_dict.items():
                for line in text_list:
                    f.write(line + '\n')
                f.write('\n')
    
    return line_text_dict

# show text_dict fucntion
def show_text_dict(line_text_dict):
    for para, text_list in line_text_dict.items():
        print(*text_list, sep='\n', end='\n\n')
        