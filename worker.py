#!/usr/bin/python
# encoding: utf-8

# worker combinator file

print('Importing library')
import torch
from src.utils import reset_output, show_text_dict
import src.crnn as crnn
from src.image_processing import line_extractor_ulti
from src.predict import predict_ulti
import os, argparse, time
import cv2

# reset output data
reset_output()

parser = argparse.ArgumentParser()
parser.add_argument('--model_path', default='models/CRNN_CTC_1.pth', help='path to model')
parser.add_argument('--imgW', type=int, default=None, help='path to model')
parser.add_argument('--imgH', type=int, default=32, help='path to model')

opt = parser.parse_args()

alphabet = open('lib/char', encoding='utf8').read().rstrip()
nclass = len(alphabet) + 1
nc = 3
print('Initializing, check for GPU cuda service')
model = crnn.CRNN(opt.imgH, nc, nclass, 256)
if torch.cuda.is_available():
    model = model.cuda()
print('Loading CRNN-CTC model from %s' % opt.model_path)
model.load_state_dict(torch.load(opt.model_path, map_location='cpu'))

def main(image):
    # image preprocessing and line extracting
    line_image_dict = line_extractor_ulti(image)
    # predict text
    text_dict = predict_ulti(line_image_dict, model, alphabet, output_file_path='output/output.txt')
    # show results
    show_text_dict(text_dict)
    
    return text_dict

while True:
    img_path = input("Image: ")
    if not os.path.isfile(img_path):
        print ("Oops. It looks like your input is incorrect. Please try again!")
        continue
    img = cv2.imread(img_path)
    tic = time.time()
    
    main(img)
    
    print(f'Predict time: {time.time()-tic}s')